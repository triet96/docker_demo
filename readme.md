# docker_provision_3_tier_webservice

## Description:
This project demonstrates how to use Docker Compose and Docker Swarm to provision a simple 3-tier webservice on a cluster of 3 AWS ec2 instances.

The webservice contains three containers: MongoDB, Nodejs Express backend and ReactJS frontend.

**NOTE** This projects assumes:
* A Cluster of 3 AWS ec2 instances running Ubuntu 18.04 in the same subnet
* Machines have ports 80 and 3000 opened to the world

## Usage:
1. Install docker as instructed [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/) on **All machines**
1. Run `docker swarm init` on the master node. Note the `docker swarm join ...` command printed in the console.
1. Run `docker swarm join ...` above on 2 slave nodes.
1. On master node, do the following steps from the repo directory:
   1. Run `docker node ls` and comfirm 3 nodes are ready
   2. Replace the value `MASTER_IP` in `docker-compose.yml` file
   3. Run `docker stack deploy -c docker-compose.yml todolist`
## Test:
Access public IP of any instances with browser
### TESTING USING COMPANY NETWORK MAY NOT WORK:
If you're testing this using KMS network, the database may not load because port 3000 is blocked so the front end can't connect to the back end. However front end will still load because port 80 is opened.

