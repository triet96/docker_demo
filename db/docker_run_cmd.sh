#!/bin/bash

#This script wraps the docker command to run the todolist_db container with specific hard-coded setting

docker run --network="todolist" --ip="10.1.5.1" --name="todolist_db" minhtrietbkit/todolist_db:latest
