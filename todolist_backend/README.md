#This is the back end of the project

#Install

1. Install mongodb: follow the instructions [here](https://docs.mongodb.com/manual/). The version in dev environment is 3.2.20
2. Install npm and nodejs with instructions [here](https://nodejs.org/en/download/package-manager/). npm version in dev environment is 6.4.1 and nodejs version is 8.9.4
3. After cloning the project, run `npm install` to automatically install packages

#Start

`sudo service mogod restart; nodejs app.js`

**Note** Password is required to restart mongod service

#Config if needed

The file `config.js` has several configurations that need to be manually configured so that they are insync with the back end project

#Neccessary packages

The necessary packages are described in the package.json file. They are installed automatically when running the command `npm install`
