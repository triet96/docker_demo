const config = require("./config");
var cors = require("cors");

const express = require("express");
const app = express();
app.use(cors());

const port = config.config.BACK_END_PORT;

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(
  `mongodb://${config.config.DB_HOST}:${config.config.DB_PORT}/${config.config.DB_NAME}`
);

const itemSchema = new mongoose.Schema({
  name: String,
  task: String
});

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const Item = mongoose.model("Item", itemSchema);

app.get("/todo", (req, res) => {
  console.log("get here");
  Item.find((err, items) => {
    if (err) return res.status(500).send(err);
    return res.status(200).send(items);
  });
});

app.post("/todo", (req, res) => {
  const myData = new Item(req.body);
  myData
    .save()
    .then(item => {
      res.send("item saved to database");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

app.listen(port,config.config.BACK_END_HOST, () => {
  console.log(`Back end host is ${config.config.BACK_END_HOST}`);
  console.log(`Back end port is ${config.config.BACK_END_PORT}`)
  console.log(`Mongodb host is ${config.config.DB_HOST}`);
  console.log(`Mongodb port is ${config.config.DB_PORT}`);
});
