const config = {
  BACK_END_HOST:'localhost',
  BACK_END_PORT: '3001',
  DB_HOST:'localhost',
  DB_PORT: '27017',
  DB_NAME: 'todolist'
};

module.exports = {
  config: config
};
