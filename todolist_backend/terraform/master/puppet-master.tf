provider "aws" {
  region     = "us-east-1"
}

# Create user_data template
data "template_file" "user_data" {
  template = "${file("bootstrap.tpl")}"
}

# Create EC2 instance
module "ec2_cluster" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name           = "Puppet-master"
  instance_count = 1

  ami                           = "ami-0ac019f4fcb7cb7e6"
  instance_type                 = "t3.medium"
  key_name                      = "EC2Keypair"
  monitoring                    = true
  vpc_security_group_ids        = ["${module.sg_test.this_security_group_id}"]
  subnet_id                     = "subnet-ce04b8e1"
  associate_public_ip_address   = true
  user_data                     = "${data.template_file.user_data.rendered}"
  tags = {
    Puppet = "master"
    Environment = "dev"
  }
}

# Create Security group add to EC2
module "sg_test" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "terra-test"
  description = "Security group for testing Terraform with custom ports open within VPC publicly open HTTP/HTTPS port 80/443"
  vpc_id      = "vpc-2e591156"

  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["http-80-tcp"]
  egress_rules             = ["all-all"]
  
  ingress_with_cidr_blocks = [
    {
      from_port   = 8140
      to_port     = 8140
      protocol    = "tcp"
      description = "http"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "ssh"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}