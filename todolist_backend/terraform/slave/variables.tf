# Set cird block as variable
variable "my_cidr_blocks" {
    default = "172.16.0.0/16"
}

# Set master hostname as variable
variable "master_hostname" {
}

output "master_hostname" {
  value = "${var.master_hostname}"
}