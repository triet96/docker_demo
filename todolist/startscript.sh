#!/bin/bash

#Description

#Start front end server with specified Front end/Back end ip/port information
#INPUT

#front-end host,port: <host>,<port>
#back-end host,port: <host>,<port>

#Default front-end host is 'localhost'
#Default front-end port is '80'

#Default back-end host is 'localhost'
#Default back-end port is '3000'


#OUTPUT
#Two files will be created/appended if existed, which are server_stdout and server_stderr.err for logging output and error of server

#Global Vars
FRONT_END_PORT_DEFAULT="80"
FRONT_END_HOST_DEFAULT="0.0.0.0"

BACK_END_PORT_DEFAULT="3000"
BACK_END_HOST_DEFAULT="localhost"

FRONT_END_PORT=""
FRONT_END_HOST=""
BACK_END_PORT=""
BACK_END_HOST=""


while getopts ":b:f:" opt; do
  case $opt in
    b)
	  read -r BACK_END_HOST BACK_END_PORT <<< $( echo "$OPTARG" | tr "," " " )
      ;;
	f)
	  read -r FRONT_END_HOST FRONT_END_PORT <<< $( echo "$OPTARG" | tr "," " " )
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

#Check port is not empty

if [[ -z "$BACK_END_HOST"  ]]; then
	echo Back-end Host input is null
	echo Use default host: "$BACK_END_HOST_DEFAULT"
else
	echo Back-end Host input is "$BACK_END_HOST"
fi

if [[ -z "$BACK_END_PORT"  ]]; then
	echo Back-end Port input is null
	echo Use default port: "$BACK_END_PORT_DEFAULT"
else
	echo Back-end Port input is "$BACK_END_PORT"
fi

BACK_END_HOST=${BACK_END_HOST:="$BACK_END_HOST_DEFAULT"}
BACK_END_PORT=${BACK_END_PORT:="$BACK_END_PORT_DEFAULT"}

if [[ -z "$FRONT_END_HOST"  ]]; then
	echo FRONT_END Host input is null
	echo Use default host: "$FRONT_END_HOST_DEFAULT"
else
	echo FRONT_END Host input is "$FRONT_END_HOST"
fi

if [[ -z "$FRONT_END_PORT"  ]]; then
	echo FRONT_END Port input is null
	echo Use default port: "$FRONT_END_PORT_DEFAULT"
else
	echo FRONT_END Port input is "$FRONT_END_PORT"
fi

FRONT_END_HOST=${FRONT_END_HOST:="$FRONT_END_HOST_DEFAULT"}
FRONT_END_PORT=${FRONT_END_PORT:="$FRONT_END_PORT_DEFAULT"}

#Modify front-end port/host in config file (right now, the Front end info in this file is not used elsewhere)
sed -ri "/FRONT_END_HOST/s/'.*'/'$FRONT_END_HOST'/" src/config.js
sed -ri "/FRONT_END_PORT/s/'.*'/'$FRONT_END_PORT'/" src/config.js
#Modify back-end port/host in config file
sed -ri "/BACK_END_HOST/s/'.*'/'$BACK_END_HOST'/" src/config.js
sed -ri "/BACK_END_PORT/s/'.*'/'$BACK_END_PORT'/" src/config.js

#Modify port in package.json file. This is the command which is used to start the server with specified port
sed -ri.bak "/start/s/PORT=.* react-scripts/PORT="$FRONT_END_PORT" react-scripts/" package.json
sed -ri.bak "/start/s/HOST=.* PORT/HOST="$FRONT_END_HOST" PORT/" package.json

#Kill processes Listening on specified port
#fuser -k -n tcp "$FRONT_END_PORT"
#Start server
echo Starting server at port "$FRONT_END_PORT"
echo =========================================
npm start #>> server_stdout.out 2>> server_stderr.err
